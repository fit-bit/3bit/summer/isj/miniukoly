#!/usr/bin/env python3
# minitask 7

def deprecated(func):
    def wrapper(*args, **kwargs):
        print("Call to deprecated function: "+func.__name__)
        print(func(*args, **kwargs))
    return wrapper

@deprecated
def some_old_function(x, y):
    return x + y

some_old_function(1,2)

# should write:
# Call to deprecated function: some_old_function
# 3 