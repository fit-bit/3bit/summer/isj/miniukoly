# minitask 5
default_qentry = ('How do you feel today?', ['sad','happy','angry'])

funcqpool = [('If return statement is not used inside the function, the function will return:',
          ['0',
           'None object',
           'an arbitrary integer',
           'Error! Functions in Python must have a return statement.'
          ]),
         ('Which of the following function calls can be used to invoke function definition:\n def test(a, b, c, d):?',          
          ['test(1, 2, 3, 4)',
           'test(a = 1, 2, 3, 4)',
           'test(a = 1, b = 2, c = 3, 4)',
           'test(a = 1, b = 2, c = 3, d = 4)',
           'test(1, 2, 3, d = 4)])'])
        ]
funcquiz1 = [('Which of the following keywords marks the beginning of the function block?',
         ['func',
          'define',
          'def',
          'func',
         ])]

def add_question(qindex, funcqpool, quiz=None):
    quiz = [default_qentry] if quiz is None else quiz

    altcodes = "ABCDEF"
    question = ""
    try:
        question = funcqpool[qindex]
    except Exception as exception:
        print("Ignoring index " + str(qindex) + " - "+ str(exception))

    answers_array = []
    answer_with_code = zip(altcodes, question[1])
    for pair in answer_with_code:
        answers_array.append(pair[0] + ": " + pair[1])
     
    quiz.append((question[0], answers_array))
    return quiz

print(add_question(0, funcqpool, funcquiz1))
print(funcquiz1)
print(add_question(0, funcqpool))